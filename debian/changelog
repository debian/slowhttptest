slowhttptest (1.9.0-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository-Browse.
    Changes-By: lintian-brush
    Fixes: lintian: upstream-metadata-file-is-missing
    See-also:
      https://lintian.debian.org/tags/upstream-metadata-file-is-missing.html
    Fixes: lintian: upstream-metadata-missing-bug-tracking
    See-also:
      https://lintian.debian.org/tags/upstream-metadata-missing-bug-tracking.html
  * Drop unnecessary dependency on dh-autoreconf.
    Changes-By: lintian-brush
  * Update standards version to 4.6.1, no changes needed.
    Changes-By: lintian-brush
    Fixes: lintian: out-of-date-standards-version
    See-also: https://lintian.debian.org/tags/out-of-date-standards-version.html

  [ Neutron Soutmun ]
  * Switch gbp to new debian/latest default branch
    * Since default branch was renamed from debian/master to debian/latest,
      therefore, switch gbp to new branch
  * New upstream version 1.9.0
  * d/patches: drop unnecessary patch
  * d/watch: switch to watch file standard version 4
    * Follow the guideline in https://wiki.debian.org/debian/watch#GitHub
  * Update standards version to 4.6.2, no changes needed.
  * d/rules: drop trailing-whitespace
  * d/copyright: update copyright year

 -- Neutron Soutmun <neutrons@debian.org>  Sat, 04 Nov 2023 15:01:05 +0700

slowhttptest (1.8.2-1) unstable; urgency=medium

  * New upstream version 1.8.2
  * Update Build-Depends on debhelper-compat level 13
    * Build depends on debhelper-compat level 13
    * Drop dh-autoreconf, it's enabled by default
    * Drop debian/compat, replaced by debhelper-compat setting in
      debian/control
  * Bump Standards-Version to 4.5.0
    * Update insecure-copyright-format-uri to secure one
    * Add explicitly Rules-Requires-Root: no
  * Switch Vcs-{Git,Browser} to Salsa
  * Drop trailing-whitespace in debian/rules
  * Replace <project> in watch file with actual project's name
  * Add debian/gbp.conf
    * Add gbp.conf follow the DEP-14 Git packaging repository layout
  * Update debian/copyright
  * Add patch to correct the application version
    * The upstream forgot to bump the version to 1.8.2
  * Add Salsa CI manifest
  * Drop d/lintian-overrides, unneeded
    * The tag `no-upstream-changelog` has been removed from lintian checking,
      therefore, no need to override

 -- Neutron Soutmun <neutrons@debian.org>  Tue, 13 Oct 2020 13:18:52 +0700

slowhttptest (1.7-1) unstable; urgency=medium

  * Imported Upstream version 1.7
  * Drop patches/fix-manpage-typo.patch, merged upstream
  * Use cgit in Vcs-Browser

 -- Neutron Soutmun <neutrons@debian.org>  Thu, 30 Jun 2016 09:19:29 +0700

slowhttptest (1.6+git20160628.da64093-1~exp1) experimental; urgency=medium

  * Imported Upstream version 1.6+git20160628.da64093 (Closes: #828548)
  * Drop debian/patches/01-fix-manpage-warning.patch, merged upstream
  * Change my email to neutrons@debian.org
  * Bump Standards-Version, update Vcs-* with secure-uri
    * Bump Standards-Version to 3.9.8, no changes needed
    * Update Vcs-* with secure-uri
  * Update Homepage, Source and watch file
    * Upstream moved to github
      - debian/control: Update Homepage
      - debian/copyright: Update Source
      - debian/watch: Update for github source
  * Update debian/copyright
  * Add debian/patches/fix-manpage-typo.patch

 -- Neutron Soutmun <neutrons@debian.org>  Tue, 28 Jun 2016 16:54:56 +0700

slowhttptest (1.6-1) unstable; urgency=low

  * Initial release (Closes: #732081)
  * debian/patches/01-fix-manpage-warning.patch: Fix manpage warning
  * debian/lintian-overrides: Upstream has no changelog
  * Revised by Dmitry Smirnov, Thanks.
    * debian/control:
      - Set debhelper version to >= 9~, exact version is unneeded.
      - Bump Standards-Version to 3.9.5, no changes.
      - Lower case the starting of short package description.
  * Revised by Prach Pongpanich, Thanks.
    * debian/control:
      - Fix Vcs-Browser broken URL

 -- Neutron Soutmun <neo.neutron@gmail.com>  Sun, 15 Dec 2013 08:36:31 +0700
